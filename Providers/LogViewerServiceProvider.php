<?php

namespace Modules\LogViewer\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\LogViewer\Events\Handlers\RegisterLogViewerSidebar;

class LogViewerServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterLogViewerSidebar::class);
    }

    public function boot()
    {
        $this->publishConfig('LogViewer', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\LogViewer\Repositories\LogViewerRepository',
            function () {
                $repository = new \Modules\LogViewer\Repositories\Eloquent\EloquentLogViewerRepository(new \Modules\LogViewer\Entities\LogViewer());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\LogViewer\Repositories\Cache\CacheLogViewerDecorator($repository);
            }
        );
// add bindings

    }
}
