@extends('layouts.master')

@section('content-header')
  <style type="text/css">
    a.llv-active {
      z-index: 2;
      background-color: #f5f5f5;
      border-color: #777;
    }

    @font-face {
      font-family: 'Glyphicons Halflings';
      src: url(//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.ttf) format("truetype"); 
    }
  </style>
  <h1>
    {{ trans('logviewer::logviewers.title.logviewers') }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('logviewer::logviewers.title.logviewers') }}</li>
  </ol>
@stop

@section('content')
  <div class="row">
    <div class="col-xs-12">

      @if(count($files) > 1)
      <div class="box box-primary">
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12">
              <div class="list-group">
              @foreach($files as $file)
                <a href="?l={{ base64_encode($file) }}"
                   class="list-group-item @if ($current_file == $file) llv-active @endif">
                  {{$file}}
                </a>
              @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif  

      <div class="box box-primary">
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12 table-container">
              @if ($logs === null)
              <div>
                Log file >50M, please download it.
              </div>
              @else
              <table id="table-log" class="table table-striped">
                <thead>
                  <tr>
                    <th>Level</th>
                    <th>Context</th>
                    <th>Date</th>
                    <th>Content</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($logs as $key => $log)
                  <tr data-display="stack{{{$key}}}">
                    <td class="text-{{{$log['level_class']}}}">
                      <span class="fa fa-{{{ $glToFa[$log['level_img']] }}}" aria-hidden="true"></span> &nbsp;
                      {{$log['level']}}
                    </td>
                    <td class="text">{{$log['context']}}</td>
                    <td class="date">{{{$log['date']}}}</td>
                    <td class="text">
                      @if ($log['stack']) 
                      <a class="pull-right expand btn btn-default btn-xs" data-display="stack{{{$key}}}">
                        <span class="fa fa-search"></span>
                      </a>
                      @endif
                
                      {{{$log['text']}}}
                
                      @if (isset($log['in_file'])) 
                      <br/>{{{$log['in_file']}}}
                      @endif
               
                      @if ($log['stack'])
                      <div class="stack" id="stack{{{$key}}}" style="display: none; white-space: pre-wrap;">
                        {{{ trim($log['stack']) }}}
                      </div>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
              <div>
                @if($current_file)
                <a href="?dl={{ base64_encode($current_file) }}">
                  <span class="fa fa-download"></span>
                  Download file
                </a>
                -
                <a id="delete-log" href="?del={{ base64_encode($current_file) }}">
                  <span class="fa fa-trash"></span> 
                  Delete file
                </a>
                  @if(count($files) > 1)
                    -
                    <a id="delete-all-log" href="?delall=true">
                      <span class="fa fa-trash"></span> 
                      Delete all files
                    </a>
                  @endif
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('core::partials.delete-modal')
@stop

@section('footer')
  <a data-toggle="modal" data-target="#keyboardShortcutsModal">
    <i class="fa fa-keyboard-o"></i>
  </a> &nbsp;
@stop

@push('js-stack')
  <?php $locale = locale(); ?>
  <script type="text/javascript">
    $(function () {
      $('.data-table').dataTable({
        "paginate": true,
        "lengthChange": true,
        "filter": true,
        "sort": true,
        "info": true,
        "autoWidth": true,
        "order": [[ 0, "desc" ]],
        "language": {
          "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
        }
      });
    });
  </script>

  <script>
    $(document).ready(function () {
      $('.table-container tr').on('click', function () {
        $('#' + $(this).data('display')).toggle();
      });
      $('#table-log').DataTable({
        "order": [1, 'desc'],
        "stateSave": true,
        "stateSaveCallback": function (settings, data) {
          window.localStorage.setItem("datatable", JSON.stringify(data));
        },
        "stateLoadCallback": function (settings) {
          var data = JSON.parse(window.localStorage.getItem("datatable"));
          if (data) data.start = 0;
          return data;
        }
      });
      $('#delete-log, #delete-all-log').click(function () {
        return confirm('Are you sure?');
      });
    });
  </script>  
@endpush