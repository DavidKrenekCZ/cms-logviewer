<?php

namespace Modules\LogViewer\Entities;

use Illuminate\Database\Eloquent\Model;

class LogViewerTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'logviewer__logviewer_translations';
}
