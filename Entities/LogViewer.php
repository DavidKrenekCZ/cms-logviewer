<?php

namespace Modules\LogViewer\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class LogViewer extends Model
{
    use Translatable;

    protected $table = 'logviewer__logviewers';
    public $translatedAttributes = [];
    protected $fillable = [];
}
