<?php

namespace Modules\LogViewer\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\LogViewer\Entities\LogViewer;
use Modules\LogViewer\Http\Requests\CreateLogViewerRequest;
use Modules\LogViewer\Http\Requests\UpdateLogViewerRequest;
use Modules\LogViewer\Repositories\LogViewerRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Rap2hpoutre\LaravelLogViewer\LaravelLogViewer;

class LogViewerController extends AdminBaseController
{
    /**
     * @var LogViewerRepository
     */
    private $logviewer;
    protected $request;
    private $glyphiconToFa = [
        "info" => "info-circle",
        "warning" => "exclamation-triangle"
    ]; 

    public function __construct(LogViewerRepository $logviewer, Request $request)
    {
        parent::__construct();

        $this->logviewer = $logviewer;
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if ($this->request->input('l')) {
            LaravelLogViewer::setFile(base64_decode($this->request->input('l')));
        }

        if ($this->request->input('dl')) {
            return $this->download(LaravelLogViewer::pathToLogFile(base64_decode($this->request->input('dl'))));
        } elseif ($this->request->has('del')) {
            app('files')->delete(LaravelLogViewer::pathToLogFile(base64_decode($this->request->input('del'))));
            return $this->redirect($this->request->url());
        } elseif ($this->request->has('delall')) {
            foreach(LaravelLogViewer::getFiles(true) as $file){
                app('files')->delete(LaravelLogViewer::pathToLogFile($file));
            }
            return $this->redirect($this->request->url());
        }

        return app('view')->make('logviewer::logviewer', [
            'logs' => LaravelLogViewer::all(),
            'files' => LaravelLogViewer::getFiles(true),
            'current_file' => LaravelLogViewer::getFileName(),
            "glToFa" => $this->glyphiconToFa
        ]);
    }

    private function redirect($to) {
        if (function_exists('redirect')) {
            return redirect($to);
        }

        return app('redirect')->to($to);
    }

    private function download($data) {
        if (function_exists('response')) {
            return response()->download($data);
        }

        // For laravel 4.2
        return app('\Illuminate\Support\Facades\Response')->download($data);
    }    
}
