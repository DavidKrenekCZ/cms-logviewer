<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/logviewer'], function (Router $router) {
    $router->bind('logviewer', function ($id) {
        return app('Modules\LogViewer\Repositories\LogViewerRepository')->find($id);
    });
    $router->get('logviewers', [
        'as' => 'admin.logviewer.logviewer.index',
        'uses' => 'LogViewerController@index',
        'middleware' => 'can:logviewer.logviewers.index'
    ]);
// append

});
