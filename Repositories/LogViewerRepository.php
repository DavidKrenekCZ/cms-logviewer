<?php

namespace Modules\LogViewer\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface LogViewerRepository extends BaseRepository
{
}
