<?php

namespace Modules\LogViewer\Repositories\Cache;

use Modules\LogViewer\Repositories\LogViewerRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheLogViewerDecorator extends BaseCacheDecorator implements LogViewerRepository
{
    public function __construct(LogViewerRepository $logviewer)
    {
        parent::__construct();
        $this->entityName = 'logviewer.logviewers';
        $this->repository = $logviewer;
    }
}
