<?php

namespace Modules\LogViewer\Repositories\Eloquent;

use Modules\LogViewer\Repositories\LogViewerRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentLogViewerRepository extends EloquentBaseRepository implements LogViewerRepository
{
}
